import React,{useState} from 'react'
import styles from "./UserItem.module.css"
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import User from './User'


const UsersItem = ({user:{ login, avatar_url, html_url, blog, location,
    followers,
    public_repos,
    public_gists,
    url,
    organizations_url,
    repos_url}}) => {
    
const[isActive, setIsActive] = useState(false);
   

    

        return (
            <div className={styles.main}>
            <main className={styles.strip}>
                
                <span className={styles.imgDisplay}>
                <img className={styles.img} alt="Avatar Url" src={avatar_url} />
                <div className={styles.profile}>
                <h3 className={styles.text}>{login}</h3>
                
                <span className={styles.normaltxt}>Profile Url: <a className={styles.profiletxt} href={url}>{url}</a></span>
                
                 <span className={styles.normaltxt}>Organization_url:<a className={styles.orgUrltbtn} href={ organizations_url} target="_blank" >Organization_url</a></span>
                <span className={styles.normaltxt}>Repos_url: <a className={styles.reposUrltbtn} href={ repos_url} target="_blank" >Repos_url</a></span>
               
               </div>

                </span>
                {/* <User
                login={login}
                avatar_url={avatar_url}
                name={name}
                /> */}
                

                <span>
                <button onClick={() => setIsActive(!isActive)} className={styles.btn}>
                
                <span>{isActive ? 'collapse' : 'Details'} </span>
                </button>
                {isActive && <> 
                <span>

                <table className={styles.tablecont}>
                    <thead className={styles.tbody}>
                        <tr>
                             <h1>User profile</h1>
                        </tr>
                    </thead>
                    <tbody  className={styles.tbody}>

                    <tr>
                    <td>
                    <a className={styles.publicrepobtn} href={public_repos} target="_blank" >Public_repos</a>
                   </td>

                   <td>
                    <a className={styles.publicgistbtn} href={ public_gists} target="_blank" >Public_gists</a>
                    </td>
                  </tr>
                     <tr>
                     <td>
                    <a className={styles.blogbtn} href={blog} target="_blank" >Blog</a>
                    </td>

                    <td>
                    <a className={styles.locationbtn} href={location} target="_blank" >Location</a>
                   </td>
                    </tr>
                   
                   <tr>
                   <td>
                    <a className={styles.htmlbtn} href={html_url} target="_blank" >HTML_url</a>
                    </td>

                    <td>
                    <a className={styles.followersbtn} href={followers} target="_blanfollowersbtn" >Followers</a>
                    </td>
                    </tr>
                   
                    </tbody>
                    </table>
  
                    
                  </span></>  }
                   
                    {/* <Link to={`/user/${login}`} className={styles.btn}>More</Link> */}
                </span>
            
           
            </main>
            </div>
            
        )
    }

    UsersItem.propTypes={
        user:PropTypes.object.isRequired,
    }


export default UsersItem

