import React from 'react';
import UsersItem from './UsersItem';
import PropTypes from 'prop-types'


 const Users = ({users}) =>  {
        return (
            <div>
                {users.map((user) => {
                    //<div>{user.login}</div>
                    return <UsersItem key={user.id} user={user}/>
                })}
            </div>
        )
    }
    // const sorted = [...users].sort((a, b) 

Users.prototype = {
   users: PropTypes.array.isRequired,
   loading: PropTypes.bool.isRequired
}
export default Users
