import React,{useEffect} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';
import Spinner from '../../Layout/Spinner';
import Repos from '../repos/Repos';
import styles from './User.module.css';


const User = ({user, loading, getUserRepos, getUser, repos, match, check, circle}) => {
    useEffect(() => {
        getUser(match.params.login);
        getUserRepos(match.params.login);
    }, []);
  // console.log(user.items,">>>><<<<<")

    const {
        organizations_url,
        repos_url,
        url,
        name,
        avatar_url,
        company,
        location,
        bio,
        blog,
        login,
        html_url,
        followers,
        following,
        public_repos,
        public_gists,
        hireable
    } = user

    if(loading){
        return <Spinner/>
    }
    return (
        <>
         <Link to='/' className={styles.btnlight}>Back To Search</Link>
         Hireable:{' '}
         {hireable ? (
             <i className={check} style={{ color: 'green', size: '50px'}}/>
         ) : (
             <i className= {circle}/>
         )} 

         <div className={styles.card}>
             <div className={styles.center}>
                 <img src={avatar_url}
                 className={styles.roundimg}
                 alt=''
                 />
                 <h1>{name}</h1>
                 <p>Location: {location}</p>
             </div>
             <div>
                 {bio && (
                     <>
                     <h3 className={styles.blog}>Bio</h3>
                     <p>{bio}</p>
                     </>
                 )}
                 <a href={html_url} classname={styles.btndark}>
                     Visit Github Profile
                 </a>
                 <ul>
                     <li>
                         {login && (
                             <>
                             <strong>Username</strong> {login}
                                 </>
                         )}
                     </li>

                     <li>
                         {company && (
                             <>
                             <strong>Company:</strong>{company}

                             </>
                         )}
                     </li>
                     <li>
                         {blog && (
                             <>
                             <strong className={styles.blog}> Website</strong>{blog}
                             </>
                         )}
                     </li>
                 </ul>
             </div>
             </div> 

             <div className={styles.cardtextcenter}>
                 <div className={styles.badgeprimary}>Followers:{followers}</div>
                 <div className={styles.badgesuccess}>Following:{following}</div>
                 <div className={styles.badgelight}>Public Repos:{public_repos}</div>
                 <div className={styles.badgedark}>Public Gist:{public_gists}</div>
            </div>
            <Repos repos={repos}/>
        </>
    )
}

User.propTypes = {
    loading: PropTypes.bool,
    user: PropTypes.object.isRequired,
    repos: PropTypes.array.isRequired,
    getUser: PropTypes.func.isRequired,
    getUserRepos: PropTypes.func.isRequired
}

User.defaultProps = {
check: 'fas fa-check',
circle:'fas fa-times-circle '
}
export default User;
