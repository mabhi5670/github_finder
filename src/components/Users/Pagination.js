import React from 'react';
import styles from './Pagination.module.css';

const Pagination = ({postsPerPage, totalPosts, paginate}) => {
 const pageNumbers = [];
 for(let index = 1; index <= Math.ceil(totalPosts / postsPerPage); index++){
     pageNumbers.push(index);
 }
    
    return (
        <nav className={styles.paginate}>
            {pageNumbers.map(number => (
               <li key={number} className="page-item">
                   <a onClick={() => paginate(number)}  className="page-link">
                       {number}
                   </a>
               </li> 
            ))}
        </nav>
    )
}

export default Pagination;
