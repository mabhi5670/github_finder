import React, {useState,useEffect}from 'react'
import PropTypes from 'prop-types'
import Dropdown from './DropDown' ;
import styles from './Search.module.css'


const Search = ({searchUsers, showClear,  clearUsers, setAlert, user, setuser}) => {
const [text, setText] = useState('');
    const onSubmit = (e) =>{
        e.preventDefault();
        if(text === '') {
            setAlert('Please enter something', 'light');
        }else{
            searchUsers(text);
            setText('');
        }
    };


    const onChange = e => setText(e.target.value);
    return (
    <div className={styles.main}>

    {/* <span className={styles.dropdn}>

        </span> */}




            {/* <span className={styles.search}> */}
           <div >
            <Dropdown
        user={user}
        setuser={setuser}
        />
        </div>
            <div>
            <form onSubmit={onSubmit} className={styles.form}>
            
            
                <input
                type='text'
                name='text'
                placeholder='Search Users...'
                value={text}
                onChange={onChange}
                className={styles.input}
                />

                <input
                type='submit'
                value='Search'
                className={styles.searchbtn}/>
                
                
                 {showClear && (
                <button className={styles.clearbtn} onClick={clearUsers}>
                    Clear
                </button>
            )}

            </form>
             </div>       

        {/* </span> */}

        {/* <span className={styles.clear}>

            </span> */}
        </div>
    )
}

Search.prototype = {
    searchUsers: PropTypes.func.isRequired,
    clearUsers: PropTypes.func.isRequired,
    showClear: PropTypes.bool.isRequired,
    setAlert: PropTypes.func.isRequired
};
export default Search;
