import React from 'react';
import {useState, useEffect} from 'react';
import styles from './DropDown.module.css'

const DropDown = ({user, setuser}) => {
    const [data, setData] = useState([]);
    const [sortType, setSortType] = useState('A_Z')

    
    useEffect(() => {
    const sortArray = type =>{
        console.log(type,"type<><<L><")
        const types = {
            A_Z: 'login',
            Z_A: 'login',
            Rank_Up: 'id',
            Rank_Down: 'id'
        };
        const sortProperty = types[type];
        if(type === 'Rank_Down' || 'Z_A' === type ){
        //const sorted = [...user].sort((a, b) => b[sortProperty] - a[sortProperty]);
        const sorted = [...user].sort((a,b) => (b[sortProperty] > a[sortProperty]) ? 1 : ((a[sortProperty] > b[sortProperty]) ? -1 : 0))

        setuser(sorted);
        console.log(sorted,"sorted>>><<<<")
        }else{
           // const sorted = [...user].sort((a, b) => a[sortProperty] - b[sortProperty]);
           const sorted = [...user].sort((a,b) => (a[sortProperty] > b[sortProperty]) ? 1 : ((b[sortProperty] > a[sortProperty]) ? -1 : 0))
            setuser(sorted);
        }
        
    }

    sortArray(sortType);
  }, [sortType]); 

    return (
        <div>
            <select onChange={(e) => setSortType(e.target.value)} className={styles.drpdn}>
            <option value="null"></option>
                <option value="A_Z" className={styles.az}>A-Z</option>
                <option value="Z_A">Z-A</option>
                <option value="Rank_Up">Rank</option>
                <option value="Rank_Down">Rank Down</option>
                </select>
        </div>
    )
}

export default DropDown;
