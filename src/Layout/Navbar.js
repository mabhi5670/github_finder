import React from "react";
import styles from "./Navbar.module.css";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Search from "../components/Users/Search";
import { Children } from "react";



const Navbar = ({children,icon, title,searchUsers,clearUsers,showClear,users,setAlert,setuser,SetAlert,setUsers}) =>{
       
        return(
            <div className={styles.navbar}>

{/* <div className={styles.navCont}> */}
<header className={styles.header}>
                <h2 className={styles.text}><i style={{ marginTop: 3 }} className={icon} />{title}</h2>
            {/* <ul> */}
            <section className={styles.child}>{children}</section>
            </header>
           
           {/* <section className={styles.route}>
                {/* <span>
                    <Link to='/'>Home</Link>
                </span>
                <span>
                    <Link to='/about'>About</Link>
                </span> 
                </section>
            </ul> */}
            {/* </div> */}
            </div>
        )
    }

Navbar.defaultProps = {
    title:"Github",
    icon:"fa fa-github "
};

Navbar.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired

};


export default Navbar;

//https://www.robinwieruch.de/react-remove-item-from-list