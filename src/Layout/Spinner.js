import React from 'react'
import styles from './Spinner.module.css'
import spinner from './spinner.gif'
const Spinner = () => {
    return (
        <>
          <img src={spinner} alt="loading..." className={styles.spinner}/> 
        </>
    )
}

export default Spinner;
