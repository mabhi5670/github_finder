import React from 'react'
import styles  from './Alert.module.css';

const Alert = ({alert}) => {
    return (
        alert !== null && (
        <div className={styles.alert}>
        <i className='fas fa-into-circle'/> {alert.msg}
        </div>
        )
    )
}

export default Alert;


