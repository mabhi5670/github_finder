import './App.css';
import React from 'react';
import Navbar from "./Layout/Navbar";
import Users from './components/Users/Users';
import axios from 'axios';
import {useState} from 'react';
import User from './components/Users/User';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Search from './components/Users/Search';
import Alert from './Layout/Alert';
import About from './components/pages/About';
import Pagination from './components/Users/Pagination';
import Responsive from './components/Responsive/Responsive';

const App= () =>  {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  const [repos, setRepos] = useState([]);
  const [loading, setLoading] = useState(false);
  const [alert, setAlert] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerpage] = useState(5);


  const { width } = Responsive()



  console.log("console.log<<<>>>")

  //Search Github User
  const searchUsers = async text =>{
    setLoading(true);
   const res = await axios.get(`https://api.github.com/search/users?q=${text}&client_id=${
     process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${ process.env.REACT_APP_GITHUB_CLIENT_SECRET}`);
     setUsers(res.data.items);
     setLoading(false);
  }
  
  
  
  //Get singal Github user
 const getUser = async username =>{
    setLoading(true);

    const res = await axios.get(
      `https://api.github.com/users/${username}?client_id=${
        process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${ process.env.REACT_APP_GITHUB_CLIENT_SECRET}`);
        setUser(res.data);
        setLoading(false);
  }

  // Github users repos
 const getUserRepos = async username => {
    setLoading(true);

    const res = await axios.get(
      `https://api.github.com/users/${username}&client_id=${
        process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${ process.env.REACT_APP_GITHUB_CLIENT_SECRET}`);
        setRepos(res.data);
        setLoading(false);
  }

  // Clear users from state
 const clearUsers = () => {
    setUsers([]);
    setLoading(false);
  };

  //Set Alert
 const  SetAlert = (msg, type) => {
    setAlert({msg, type});
    setTimeout(() => setAlert(null), 5000);
  }

  //Get Current Posts
  const indexOfLastPost = currentPage * postsPerpage;
  const indexOfFirstPost = indexOfLastPost - postsPerpage;
  const currentPosts = users.slice(indexOfFirstPost, indexOfLastPost);
console.log(currentPosts,"currentPostsl>><<<<<")

const paginate = (pageNumber) => setCurrentPage(pageNumber)
console.log(paginate,"paginate<<>><<<")

const total_result=users.length;
  console.log("total posts",total_result);

   return (
     <Router>
    <div className="App">
     <Navbar><Search
           searchUsers={searchUsers}
           clearUsers={clearUsers}
           showClear={users.length > 0 ? true : false}
           setAlert={SetAlert}
           user={users}
           setuser={setUsers}
           /></Navbar>
           
     <div className='container'>
       <Alert alert={alert}/>
       <Switch>
         <Route exact path='/'
         render={props => (
           <>
           
           <span>Total Result:{total_result}</span>
           <Users loading={loading} users={currentPosts}/>
           <Pagination
           postsPerPage={postsPerpage} totalPosts={users.length} paginate={paginate}/>
           </>
         )}
         />
         <Route exact path='/about' component={About}/>
         <Route
         exact path='/user/:login'
         render={props =>(
           <User
           {...props}
           getUser={getUser}
           getUserRepos={getUserRepos}
           user={users}
           repos={repos}
           loading={loading}
           />
         )}
         />
       </Switch>
     </div>
    </div>
    </Router>
  );
}

export default App;
